#helper functions for pacman 
import numpy as np
from random import randint, choice
import time
import math

pacman_x_old = 0
pacman_y_old = 0
choices = []
previous_choice = []



# -----------------Main Control Logic ---------------------------------------------------
def brains(locations):

    global pacman_x_old
    global pacman_y_old
    global previous_choice
    choices = []

    nearest = find_nearest(locations)
    # ghost colors: orange, red, cyan, purple
    #print(locations['pacman'])
    #next_action = randint(0,4)

    #nearest = "ghost_X"



    # if locations['pacman'][0] < locations[nearest][0]: 
    #     print nearest + " is on right!"
    #     next_action = 3
    #     choices.append(3)
    # else: 
    # 	print nearest + " is on left!"
    #     next_action = 2
    #     choices.append(2)

    # if locations['pacman'][1] < locations[nearest][1]: 
    # 	print nearest + " is down!"    	
    #     next_action = 1
    #     choices.append(1)

    # else: 
    # 	print nearest + " is up!"
    #     next_action = 4
    #     choices.append(4)




    # #same location as previous time; we're stuck.
    # if(locations['pacman'][1] == pacman_y_old and locations['pacman'][0] == pacman_x_old ):  
    # 	choices = []
    # 	if(previous_choice==1 ): choices.append(4) # we went UP so now try DOWN (or vice versa)
    #     else:    choices.append(1)
    #     if(previous_choice==2 ): choices.append(3) #we tried RIGHT so now try LEFT (or vice versa)
    #     else:    choices.append(2)


    # pacman_x_old = locations['pacman'][0] 
    # pacman_y_old = locations['pacman'][1]  
    # next_action = choice(choices)
    # previous_choice = next_action
    # choices = [] 


    #time.sleep(0.1)
    next_action = None         #  If no action, use Human Intelligence (tm), e.g. keyboard keys W,S,A,D
    #next_action = randint(0,4)  # Select randomly: up(1), right(2), left(3), down(4) or no change (0) 





    return next_action

# -----------------EndOf Main Control Logic ---------------------------------------------------


# Returns ghost_name nearest to player
def find_nearest(locations):
    nearest = 'none'
    d1 =  math.hypot(locations['pacman'][0] - locations['ghost_orange'][0], locations['pacman'][1] - locations['ghost_orange'][1])
    d2 =  math.hypot(locations['pacman'][0] - locations['ghost_red'][0], locations['pacman'][1] - locations['ghost_red'][1])
    d3 =  math.hypot(locations['pacman'][0] - locations['ghost_cyan'][0], locations['pacman'][1] - locations['ghost_cyan'][1])
    d4 =  math.hypot(locations['pacman'][0] - locations['ghost_purple'][0], locations['pacman'][1] - locations['ghost_purple'][1])
    if min(d1,d2,d3,d4) == d1: 
    	nearest = "ghost_orange"
    if min(d1,d2,d3,d4) == d2: 
    	nearest = "ghost_red"
    if min(d1,d2,d3,d4) == d3: 
    	nearest = "ghost_cyan"
    if min(d1,d2,d3,d4) == d4: 
    	nearest = "ghost_purple"

    return nearest
