import numpy as np
import gym
import time
import math
from pacman_helpers import *
import csv
import os.path

env = gym.make("MsPacman-v0")
historyfile = "history.csv"
render=True


agent_action = 0
previous_lives = 3
row = 0;
steps=0
quit = False

def key_press(key, mod):
    global agent_action, quit
    if key==115: agent_action = 4  # down
    if key==97: agent_action = 3   # left
    if key==100: agent_action = 2  # right 
    if key==119: agent_action = 1  # up
    if key==65307: quit = True     # esc
def key_release(key, mod):
    global agent_action
    a = int( key - ord('0') )
    if agent_action == a:
        agent_action = 0


def character_locations(I):
  # returs a data structure storing game sprite locations

  I = I[1:171] # crop play area from image
  #I = I[::2,::2,0] # downsample by factor of 2


  I[I == 28] = 0 # erase background walls by color
  I[I == 111] = 0 
  I[I == 136] = 0 
  I[I == 228] = 0 

  pacman_img = np.copy(I)  
  ghost1_img = np.copy(I) 
  ghost2_img = np.copy(I)
  ghost3_img = np.copy(I)
  ghost4_img = np.copy(I)
  ghost5_img = np.copy(I)
  pacman_img[pacman_img != 164] = 0
  ghost1_img[ghost1_img != 48] = 0  #ORANGE ghost
  ghost2_img[ghost2_img != 72] = 0  #RED ghost
  ghost3_img[ghost3_img != 84] = 0  #CYAN ghost
  ghost4_img[ghost4_img != 179] = 0 #PURPLE ghost

  # obtain x,y locations for each character finding non-zero areas
  pac_x = np.argmax(np.max(pacman_img, axis=0))
  pac_y = np.argmax(np.max(pacman_img, axis=1))
  g1_x = np.argmax(np.max(ghost1_img, axis=0))
  g1_y = np.argmax(np.max(ghost1_img, axis=1))
  g2_x = np.argmax(np.max(ghost2_img, axis=0))
  g2_y = np.argmax(np.max(ghost2_img, axis=1))
  g3_x = np.argmax(np.max(ghost3_img, axis=0))
  g3_y = np.argmax(np.max(ghost3_img, axis=1))
  g4_x = np.argmax(np.max(ghost4_img, axis=0))
  g4_y = np.argmax(np.max(ghost4_img, axis=1))


  # Store locations into datastructure, return it
  data = np.zeros(2, dtype={'names':('pacman', 'ghost_orange','ghost_red','ghost_cyan','ghost_purple'),
                          'formats':('i4','i4','i4','i4','i4')})
  data['pacman'] = (pac_x,pac_y)
  data['ghost_orange'] = (g1_x,g1_y)
  data['ghost_red'] =    (g2_x,g2_y)
  data['ghost_cyan'] =   (g3_x,g3_y)
  data['ghost_purple'] = (g4_x,g4_y)

  return data



# initial observation of game state
observation = env.reset()

while not quit:
  if render: 
      env.render()  
      env.unwrapped.viewer.window.on_key_press = key_press
      env.unwrapped.viewer.window.on_key_release = key_release


  # obtain new game state observation
  observation, reward, done, info = env.step(agent_action)
  locations = character_locations(observation)
  next_action = brains(locations)
  steps = steps + 1


  # If we got None as next action from brains, use manual human control
  if(next_action != None): agent_action = next_action
  


  # Write data of pacman and ghost locations into CSV file for further analysis
  row = {
        'pac_x': locations['pacman'][0], 'pac_y': locations['pacman'][1],
        'g_ora_x': locations['ghost_orange'][0], 'g_ora_y': locations['ghost_orange'][1],
        'g_red_x': locations['ghost_red'][0], 'g_red_y': locations['ghost_red'][1],
        'g_cyan_x': locations['ghost_cyan'][0], 'g_cyan_y': locations['ghost_cyan'][1],
        'g_purp_x': locations['ghost_purple'][0], 'g_purp_y': locations['ghost_purple'][1],
        'next_action': agent_action,
        'lives': info['ale.lives']-1,
        'steps': steps

        }

  if previous_lives != info['ale.lives']:
    steps = 0
    if info['ale.lives']==0: row_old['lives']=0

    file_exists = os.path.isfile(historyfile)
    with open(historyfile, 'a') as csvfile:
        fieldnames = ['pac_x', 'pac_y', 'g_ora_x', 'g_ora_y', 'g_red_x', 'g_red_y', 'g_cyan_x', 'g_cyan_y', 'g_purp_x', 'g_purp_y', 'next_action', 'lives', 'steps' ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        if not file_exists:
            writer.writeheader()  # new file, so write a header for it
        writer.writerow(row_old)

  # Only update row, if there are ghosts in the screen (avoids saving periods of animation after player death)
  if not (locations['ghost_orange'].all() == locations['ghost_red'].all() ==  locations['ghost_cyan'].all() ==  locations['ghost_purple'].all()):    
           row_old = row

  previous_lives = info['ale.lives']

  # EndOf CSV write

     



  if done==True or quit==True:

    # User pressed ESC or player lives ran out: exit.

    break


